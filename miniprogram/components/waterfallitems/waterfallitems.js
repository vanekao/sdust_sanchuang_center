// components/waterfallitems/waterfallitems.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    data: Object
  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    previewImage: function (e) {
      var current = e.target.dataset.src;
      console.log(current);
      let t = []
      t[0] = current
      wx.previewImage({
        current: current, // 当前显示图片的http链接  
        urls: t // 需要预览的图片http链接列表  
      })
    },
  }
})