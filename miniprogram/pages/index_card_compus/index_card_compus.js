// pages/index_card_compus/index_card_compus.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    imgs: ['https://www.sdust.edu.cn/images/banner2021040801.jpg', 'http://taxq.sdust.edu.cn/images/ba-2.jpg', 'https://636c-cloud1-4gaojii5e1c8c56e-1305568725.tcb.qcloud.la/img_show/scenery/atsea.jpg?sign=f2205cac7a3643dca811eb303f48e920&t=1622523095', 'http://taxq.sdust.edu.cn/images/banner3.png', 'https://636c-cloud1-4gaojii5e1c8c56e-1305568725.tcb.qcloud.la/img_show/scenery/asdfas.jpg?sign=23f8a4a6584971ace8c411c671307504&t=1622523217'],
    contentA: '山东科技大学建校于1951年，是一所工科优势突出，行业特色鲜明，工学、理学、管理学、文学、法学、经济学、艺术学等多学科相互渗透、协调发展的山东省重点建设应用基础型人才培养特色名校和高水平大学“冲一流”建设高校。学校在青岛、泰安、济南三地办学，设有教学单位32个，科研单位5个。有博士后科研流动站9个，博士学位授权一级学科10个，硕士学位授权一级学科27个，硕士专业学位类别16个，本科专业93个。现有全日制本科在校生34300余人，研究生8300余人。',
    contentB: '山东科技大学在泰安设泰安校区党工委和管委，负责校区党政工作。',
    contentC: '泰安校区占地750余亩，校舍面积38万余平方米。现设资源学院、智能装备学院、财经学院、公共课教学部、继续教育学院等5个教学单位，设采矿工程研究院、煤矿充填开采国家工程实验室等2个科研机构，与重庆翔美教育投资有限公司联合举办山东科技大学泰山科技学院。现有25个本科专业。有全日制在校学生9200余人，在职教职工630余人，高级职称人员170余人，博士生导师和硕士生导师60余人。有国家级、省级实验室（中心）6个，其中，“煤矿充填开采国家工程实验室”是国家发展和改革委员会批准建设的国家级实验室；“矿业工程综合实训中心”为省级实验教学示范中心；“矿业工程实验中心”和“矿山机电工程实验中心”为山东省骨干学科实验室；“先进自动化技术实验中心”为中央与地方共建特色优势学科实验室；校内教学模拟矿井被原国家煤炭部命名为“华东煤炭院校泰安实习中心”。',
    contentD: '校区大力实施“教学质量生命线工程”，不断加强专业建设、课程建设、科技创新平台建设和师资队伍建设，努力提高人才培养质量。现有国家级精品课程1门，山东省特色专业建设点3个、在建山东省高水平应用型专业2个，山东省一流课程1门、精品课程29门、在线开放课程平台上线课程4门，山东省教学团队2个，山东省教学名师2名，全国煤炭教学名师1名。校区教师参加各级各类教学比赛屡获佳绩，获国家级一等奖1项，省级奖励6项。',
    contentE: '近五年，校区承担国家自然科学基金、国家社会科学基金等国家级项目20项，山东省自然科学基金、山东省重点研发计划、山东省社科规划研究等省部级项目24项。获得省部级以上科研奖励30余项，授权专利146项，科研合同经费年均超3000万元。先后成立“泰安市岩土工程研究院”“泰山复合材料研究院”“泰安市地质与环境工程（技术）研究院”等科研创新平台。',
    contentF: '校区坚持立德树人根本任务，始终把服务和促进学生健康成长作为一切工作的出发点和落脚点，努力提高人才培养质量。多年来，毕业生就业率均在95%以上，就业质量不断提升。本科毕业生考研升学率30%以上。大学生科技创新硕果累累，2019年参加“挑战杯”等100余项校外学生科技创新竞赛，获国家级奖励290余项、省级奖励500余项，学生发表论文200余篇。',
    contentG: '下一步，校区将基于泰安的区位优势和现有资源状况，着力打造“五大基地”，即面向煤炭行业的应用型人才培养基地、学生实习实训基地、科技创新与研发基地、科技成果孵化转化基地以及继续教育与高端培训基地，培养更多“厚基础、精专业、重实践、强创新、高素质”的应用创新型人才！'
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})