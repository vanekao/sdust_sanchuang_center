// pages/prj_detail/prj_detail.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    prj: []
  },

  toAdd() {
    var _id = this.data.prj._id
    wx.navigateTo({
      url: '../add_prj_detail/add_prj_detail?id=' + _id,
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // console.log(options.prj);
    wx.showLoading({
      title: '加载中',
    })
    var prj = JSON.parse(options.prj);
    wx.cloud.callFunction({
      name: 'get_cloud_prj_by_id',
      data: {
        _id: prj._id
      }
    }).then(res => {
      wx.hideLoading({
        success: (res) => {
          
        },
      })
      this.setData({
        prj: res.result.data
      })
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    wx.showLoading({
      title: '加载中',
    })
    var prj = this.data.prj;
    wx.cloud.callFunction({
      name: 'get_cloud_prj_by_id',
      data: {
        _id: prj._id
      }
    }).then(res => {
      wx.hideLoading({
        success: (res) => {
          
        },
      })
      this.setData({
        prj: res.result.data
      })
      console.log(res);
    }).catch(err => {
      console.log(err);
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})