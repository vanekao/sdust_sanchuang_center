// miniprogram/pages/login.js
const app = getApp();
const db = wx.cloud.database();
const _ = db.command;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    isLogin: null,
  },
  confirm(e) {
    var isTest = e.currentTarget.dataset.test
    wx.getUserProfile({
      desc: '用于完善会员资料',
      success: (res) => {
        app.globalData.userInfo.nickName = res.userInfo.nickName;
        app.globalData.userInfo.avatarUrl = res.userInfo.avatarUrl;
        app.globalData.isLogin = true;
        if(isTest === 'true') {
          app.globalData.admin = true;
          app.globalData.name = "王晓"; //真实姓名
          app.globalData.gender = "男"; //性别
          app.globalData.college = "智能装备学院"; //学院
          app.globalData.class = "计算机科学与技术19-1"; //专业年级
          app.globalData.studentID = "201923020145"; //学号
          app.globalData.phoneNumber = "18253809999"; //手机号
          app.globalData.province = "山东"; //籍贯
          app.globalData.politicalStatus = "党员"; //政治面貌,
        } else {
          app.globalData.admin = false;
        }
        wx.setStorageSync('isLogin', true)
        db.collection('userlist').where({
          _openid: app.globalData.openid
        }).get().then(res => {
          if (res.data.length === 0) {
            db.collection('userlist').add({
              data: {
                openid: app.globalData.openid,
                nickName: app.globalData.userInfo.nickName,
                avatarUrl: app.globalData.userInfo.avatarUrl,
                admin: app.globalData.admin,
                name: app.globalData.name = "王晓", //真实姓名
                gender: app.globalData.gender = "男", //性别
                college: app.globalData.college = "智能装备学院", //学院
                class: app.globalData.class = "计算机科学与技术19-1", //专业年级
                studentID: app.globalData.studentID = "201923020145", //学号
                phoneNumber: app.globalData.phoneNumber = "18253809999", //手机号
                province: app.globalData.province = "山东", //籍贯
                politicalStatus: app.globalData.politicalStatus = "党员", //政治面貌,
              }
            })
          }
        })
        // db.collection('userlist').add({
        //   data: {
        //     openid: app.globalData.openid,
        //     nickName: res.userInfo.nickName,
        //     avatarUrl: res.userInfo.avatarUrl
        //   }
        // }).then(res => {
        //   console.log("保存用户数据成功");
        // })
        wx.switchTab({
          url: '/pages/index/index',
          fail: (res) => {
            console.log(res);
            console.log("拒绝授权");
          }
        })
      },
      fail: (res) => {
        wx.showToast({
          title: '未授权无法进入',
          icon: 'error',
          duration: 2000
        })


      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})