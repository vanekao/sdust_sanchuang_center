const db = wx.cloud.database()
// pages/reserve/reserve.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    dialogShow: false,
    buttons: [{
      type: 'primary',
      size: 'large',
      className: 'large-btn',
      text: '主操作',
      value: 1
    }],
    date: '2021-05-16',
    startTime: '13:00',
    endTime: '15:00',
    seats: [],
    slctSeats: '',
    selected: [],
    isSelected: null,
  },

  selected(e) {
    let t = [];
    this.setData({
      slctSeats: e.target.dataset.id
    })

    t[e.target.dataset.num] = true
    this.setData({
      selected: t,
      isSelected: e.target.dataset.num
    })
  },

  confirm() {
    wx.showToast({
      title: '即将上线',
      icon: 'error'
    })
  },

  bindDateChange: function (e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      date: e.detail.value
    })
  },
  bindStartTimeChange: function (e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      startTime: e.detail.value
    })
  },
  bindEndTimeChange: function (e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      endTime: e.detail.value
    })
  },
  openDialog() {

    if(this.getSeats() === false) return;

    if (this.data.dialogShow === false) {
      console.log(this.data.dialogShow);
      this.setData({
        dialogShow: true
      })
    } else {
      console.log(this.data.dialogShow);
      this.setData({
        dialogShow: false
      })
    }

  },
  dialogOpened(e) {
    console.log(e);
  },

  checkTime(start, end2) {
    if(start >= end2) return true;
    return false;
  },

  getSeats() {
    wx.showLoading({
      title: '正在查询',
    })
    var date = new Date();
    var rsvDate = this.data.date.split('-');
    // if(date.getFullYear() > parseInt(rsvDate[0]) ||
    //   date.getMonth() + 1 > parseInt(rsvDate[1]) ||
    //   date.getDate() > parseInt(rsvDate[2])) {
    //     wx.showToast({
    //       title: '日期失效',
    //       icon: 'error'
    //     })
    // }
    if(rsvDate[1][0] === '0') {
      rsvDate[1] = rsvDate[1][1];
    }
    // console.log(rsvDate[1]);
    if(rsvDate[2][0] === '0') {
      rsvDate[2] = rsvDate[2][1];
    }
    // console.log(rsvDate[2]);
    if(date.getFullYear() > parseInt(rsvDate[0])) {
      wx.showToast({
        title: '日期错误',
        icon: 'error'
      })
      return false;
    }
    if(date.getFullYear() <= parseInt(rsvDate[0]) &&
      date.getMonth() + 1 > parseInt(rsvDate[1])) {
      wx.showToast({
        title: '日期错误',
        icon: 'error'
      })
      return false;
    }
    if(date.getFullYear() <= parseInt(rsvDate[0]) &&
      date.getMonth() + 1 <= parseInt(rsvDate[1]) &&
      date.getDate() > parseInt(rsvDate[2])) {
      wx.showToast({
        title: '日期错误',
        icon: 'error'
      })
      return false;
    }
    var rsvStart = this.data.startTime.split(':');
    if(date.getHours() > parseInt(rsvStart[0])) {
      wx.showToast({
        title: '时间错误',
        icon: 'error'
      })
      return false;
    }
    if(rsvStart[0][0] === '0') {
      rsvStart[0] = rsvStart[0][1];
    }
    if(rsvStart[1][0] === '0') {
      rsvStart[1] = rsvStart[1][1];
    }
    if(date.getHours() <= parseInt(rsvStart[0]) && 
      date.getMinutes() > parseInt(rsvStart[1])) {
        // console.log(222);
      wx.showToast({
        title: '时间错误',
        icon: 'error'
      })
      return false;
    }
    wx.cloud.callFunction({
      name: 'getSeats'
    }).then(res => {
      console.log(res);
      wx.hideLoading({
        success: (res) => {
          console.log(res);
        },
      })

      var k = 0;
      var allSeats = res.result.data;
      // var validSeats = [];

      // console.log(allSeats);

      // for(var i = 0; i < allSeats.length; i++) {
      //   //数据库传来的预定结束时间
      //   var rsvEnd2 = allSeats[i].rsvEndTime.split(':');
      //   console.log(rsvEnd2);
      //   if(rsvEnd2[0][0] === '0') {
      //     rsvEnd2[0] = rsvEnd2[0][1];
      //   }
      //   if(rsvEnd2[1][0] === '0') {
      //     rsvEnd2[1] = rsvEnd2[1][1];
      //   }
      //   if(this.checkTime(rsvStart, rsvEnd2)) {
      //     validSeats[k++] = allSeats[i];
      //   }
      // }

      this.setData({
        seats: allSeats
      })
    }).catch(err => {
      console.log(err);
    })
  },

  setCurDateAndTime() {
    var curTime = new Date();
    var year = curTime.getFullYear();
    var month = curTime.getMonth() + 1;
    var day = curTime.getDate();
    var hour = curTime.getHours();
    var hour2 = hour + 1;
    var minute = curTime.getMinutes();
    if ((month + '').length === 1) {
      month = '0' + month;
    }
    if ((day + '').length === 1) {
      day = '0' + day;
    }
    if ((minute + '').length === 1) {
      minute = '0' + minute;
    }
    var date = year + '-' + month + '-' + day;
    var time = hour + ':' + minute;
    var endTime = hour2 + ':' + minute;
    this.setData({
      date: date,
      startTime: time,
      endTime: endTime
    })
    return true;
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setCurDateAndTime();
  },


  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.setCurDateAndTime();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})