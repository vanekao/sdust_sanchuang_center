let app = getApp();  
Page({  
data: {  
isSubmit: false,  
warn: "",  
phone: "",     
sex: "男"  ,
name:"哈哈",
college:"",
profession:"",
hobby:"",
classroom:""
},  
formSubmit: function (e) {  
console.log('form发生了submit事件，携带数据为：', e.detail.value);  
let { phone, sex ,name,college, classroom,profession,hobby,} = e.detail.value;  

if (!phone ) {  
this.setData({  
warn: "手机号不能为空！",  
isSubmit: true  
})  
return;  
}  
else if (!name || !college) {  
  this.setData({  
  warn: "姓名或学院不能为空！",  
  isSubmit: true  
  })  
  return;  
  } 

else if (!profession || !classroom) {  
    this.setData({  
    warn: "班级或专业不能为空！",  
    isSubmit: true  
    })  
    return;  
    } 
else {
  wx.showToast({
    title: '提交成功',
  })
}

 
this.setData({  
warn: "",  
isSubmit: true,  
phone,   
sex,
name,
college,
classroom,
profession,
hobby,

})  
},  
formReset: function () {  
console.log('form发生了reset事件')  
}  
})