//index.js
const app = getApp()
const db = wx.cloud.database();

Page({
  data: {
    default_img: 'cloud://cloud1-0g9lfjblead9406a.636c-cloud1-0g9lfjblead9406a-1305568725/default_img.jpeg',
    campus_img_title: 'https://636c-cloud1-4gaojii5e1c8c56e-1305568725.tcb.qcloud.la/img_show/scenery/gdg2.jpg?sign=b7740c712ce8ae670bd03ad8e361e51b&t=1622459012',
    nav_table_img: [],
    swiper_img: [],
    sanchaung_img_title: 'https://636c-cloud1-4gaojii5e1c8c56e-1305568725.tcb.qcloud.la/imgs/index_imgs/120CBFC6B24925D12C4A8483CCBD3617.jpg?sign=7ebc025d0f8b5fceb52250188523481b&t=1622508439',
    achieve_img: []
  },

  switchToRsv() {
    wx.switchTab({
      url: '../reserve/reserve',
    })
  },
  switchToMng() {
    wx.switchTab({
      url: '../manage/manage',
    })
  },
  // cardSwiper
  cardSwiper(e) {
    this.setData({
      cardCur: e.detail.current
    })
  },

  jump(e) {
    var num = e.currentTarget.dataset.lab;
    wx.navigateTo({
      url: '../lab_' + num + '/lab_' + num
    })
  },

  //nums表示轮播图数量
  getSwiperImgs(nums) {
    var imgs = []
    for(var i = 1; i <= nums; i++) {
      wx.cloud.getTempFileURL({
        fileList: ['cloud://cloud1-0g9lfjblead9406a.636c-cloud1-0g9lfjblead9406a-1305568725/首页/轮播图/b ('+i+').jpg'],
        success: res => {
          imgs.push(res.fileList[0])
          this.setData({
            swiper_img: imgs
          })
        },
        fail: console.error
      })
    }
  },

  getNavTableImgs(nums) {
    var imgs = []
    for(var i = 1; i <= nums; i++) {
      wx.cloud.getTempFileURL({
        fileList: ['cloud://cloud1-4gaojii5e1c8c56e.636c-cloud1-4gaojii5e1c8c56e-1305568725/imgs/index_nav_table_imgs/'+ i +'.jpg'],
        success: res => {
          imgs.push(res.fileList[0])
          this.setData({
            nav_table_img: imgs
          })
        },
        fail: console.error
      })
    }
  },

  getAchieveImgs(nums) {
    var imgs = []
    for(var i = 1; i <= nums; i++) {
      wx.cloud.getTempFileURL({
        fileList: ['cloud://cloud1-0g9lfjblead9406a.636c-cloud1-0g9lfjblead9406a-1305568725/实验室/成果/所有成果照片/0 ('+Math.ceil(Math.random()*128)+').jpg'],
        success: res => {
          imgs.push(res.fileList[0])
          this.setData({
            achieve_img: imgs
          })
        },
        fail: console.error
      })
    }
  },

  changePics() {
    this.getAchieveImgs(10);
  },

  //监听页面加载
  onLoad: function (options) {
    this.getSwiperImgs(4);
    this.getNavTableImgs(5);
    this.getAchieveImgs(10);
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    let state = app.globalData.isLogin
    if (state !== true) {
      wx.redirectTo({
        url: '/pages/login/login',
      })
    }
  }
})