const db = wx.cloud.database()
const _ = db.command
// pages/nav_mine/admin_page/admin_page.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    name: '',
    apply_list: [],
    lab_prob_list: [],
    advices: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      name: getApp().globalData.userInfo.name
    })
    db.collection('apply').get().then(res => {
      this.setData({
        apply_list: res.data
      })
    })
    db.collection('lab_problems').get().then(res => {
      this.setData({
        lab_prob_list: res.data
      })
    })
    db.collection('advices').get().then(res => {
      this.setData({
        advices: res.data
      })
    })
  },

  //待优化
  resp(e) {
    var index = e.target.dataset.index
    var msg = e.target.dataset.msg
    if(msg === 'accept') {
      msg = true
    } else {
      msg = false
    }
    this.data.apply_list[index].read = true
    this.data.apply_list[index]['accept'] = msg

    if(msg == true && this.data.apply_list[index].type == 'join_lab') {

      db.collection('userlist').where({
        _openid: this.data.apply_list[index]._openid
      }).update({
        data: {
          apply: _.set(this.data.apply_list),
          in_lab: this.data.apply_list[index].lab
        }
      }).then(res => {
        console.log(res);
      })

    } else if(msg == true && this.data.apply_list[index].type == 'apply_admin') {
      db.collection('userlist').where({
        _openid: this.data.apply_list[index]._openid
      }).update({
        data: {
          apply: _.set(this.data.apply_list),
          admin: true
        }
      }).then(res => {
        console.log(res);
      })
    }

    console.log(this.data.apply_list[index]._id);

    db.collection('apply').where({
      _id: this.data.apply_list[index]._id
    }).update({
      data: {
        accept: msg,
        read: true
      }
    }).then(res => {
      console.log(res);
      this.onLoad()
    })
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})