const db = wx.cloud.database()
const _ = db.command
// pages/applyto_submit/applyto_submit.js
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  submit(e) {
    wx.showLoading({
      title: '正在提交',
    })
    var text = e.detail.value.text
    db.collection('userlist').where({
      _openid: getApp().globalData.userInfo._openid
    }).get().then(res => {
      var userInfo = res.data[0]
      var apply = userInfo.apply
      if(apply === undefined) {
        apply = []
      }

      for(var i = 0; i < apply.length; i++) {
        if(JSON.stringify(apply[i]).search('apply_admin') != -1) {
          wx.showToast({
            title: '请勿重复申请',
            icon: 'error'
          })
          return;
        }
      }
      apply.push({
        type: 'apply_admin',
        name: getApp().globalData.userInfo.name,
        detail: text,
        read: false
      })

      db.collection('apply').add({
        data: {
          type: 'apply_admin',
          name: getApp().globalData.userInfo.name,
          detail: text,
          read: false
        }
      }).then(res => {
        console.log(res);
      })

      db.collection('userlist').where({
        _openid: getApp().globalData.userInfo._openid
      }).update({
        data: {
          apply: _.set(apply)
        }
      }).then(res => {
        wx.hideLoading({
          success: (res) => {
            wx.showToast({
              title: '提交成功',
              icon: 'success'
            })
          },
        })
      })
    })
    
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})