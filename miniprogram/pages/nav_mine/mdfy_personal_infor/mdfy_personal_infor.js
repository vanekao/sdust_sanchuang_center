// pages/mdfy_personal_infor/mdfy_personal_infor.js
const app = getApp();
const db = wx.cloud.database();
const _ = db.command;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userInfo: {
      name: '',
      gender: '',
      college: '',
      class: '',
      studentID: '',
      phoneNumber: '',
      province: '',
      politicalStatus: '',
    },
    man: "男",
    woman: "女",
  },
  changeName(e) {
    let name = e.detail.value
    let userInfo = this.data.userInfo
    userInfo.name = name
    this.setData({
      userInfo: userInfo
    })
  },
  changeGender(e) {
    let gender = e.detail.value
    let userInfo = this.data.userInfo
    userInfo.gender = gender
    this.setData({
      userInfo: userInfo
    })
  },
  changeCollege(e) {
    let college = e.detail.value
    let userInfo = this.data.userInfo
    userInfo.college = college
    this.setData({
      userInfo: userInfo
    })
  },
  changeClass(e) {
    let class1 = e.detail.value
    let userInfo = this.data.userInfo
    userInfo.class = class1
    this.setData({
      userInfo: userInfo
    })
  },
  changeStudentID(e) {
    let studentID = e.detail.value
    let userInfo = this.data.userInfo
    userInfo.studentID = studentID
    this.setData({
      userInfo: userInfo
    })
  },
  changePhoneNumber(e) {
    let phoneNumber = e.detail.value
    let userInfo = this.data.userInfo
    userInfo.phoneNumber = phoneNumber
    this.setData({
      userInfo: userInfo
    })
  },
  changeProvince(e) {
    let province = e.detail.value
    let userInfo = this.data.userInfo
    userInfo.province = province
    this.setData({
      userInfo: userInfo
    })
  },
  changePoliticalStatus(e) {
    let politicalStatus = e.detail.value
    let userInfo = this.data.userInfo
    userInfo.politicalStatus = politicalStatus
    this.setData({
      userInfo: userInfo
    })
  },

  submit() {
    app.globalData.userInfo = this.data.userInfo
    db.collection('userlist').where({
      _openid: app.globalData.openid
    }).update({
      data: {
        name: this.data.userInfo.name,
        gender: this.data.userInfo.gender,
        college: this.data.userInfo.college,
        class: this.data.userInfo.class,
        studentID: this.data.userInfo.studentID,
        phoneNumber: this.data.userInfo.phoneNumber,
        province: this.data.userInfo.province,
        politicalStatus: this.data.userInfo.politicalStatus,
      }
    }).then(res => {
      console.log(res);
      console.log(app.globalData);
    }).catch(res => {
      console.log(res);
      console.log(app.globalData);
    })
    wx.navigateBack({
      delta: 1,
    })
  },
  reset() {
    let userInfo = {
      name: '',
      gender: '',
      college: '',
      class: '',
      studentID: '',
      phoneNumber: '',
      province: '',
      politicalStatus: '',
    }
    this.setData({
      userInfo: userInfo
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      userInfo: app.globalData.userInfo
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.setData({
      userInfo: app.globalData.userInfo
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})