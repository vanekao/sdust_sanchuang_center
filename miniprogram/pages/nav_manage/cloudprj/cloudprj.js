const db = wx.cloud.database();

// pages/cloudprj/cloudprj.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    show_popup: false,
    prjs: [],
    isVerified: false,
    isAdmin: false,
  },

  show() {
    this.setData({
      show_popup: true
    })
  },

  switch(e) {
    var index = e.currentTarget.dataset.index;
    var queryBean = JSON.stringify(this.data.prjs[index]);
    wx.navigateTo({
      url: '../prj_detail/prj_detail?prj=' + queryBean,
    })
  },

  add_cloud_prj(res) {
    if(this.data.isVerified === false) {
      wx.showToast({
        title: '请先验证身份',
        icon: 'error'
      })
      return;
    } else if(this.data.isVerified === true && this.data.isAdmin === false) {
      wx.showToast({
        title: '权限不足',
        icon: 'error'
      })
      return;
    }
    var value = res.detail.value;
    wx.showLoading({
      title: '添加中',
    })
    wx.cloud.callFunction({
      name: 'add_cloud_prj',
      data: {
        value: value
      }
    }).then(res => {
      wx.hideLoading({
        success: (res) => {
          wx.showToast({
            title: '添加成功',
            icon: 'success'
          })
          this.setData({
            show_popup: false
          })
          this.onLoad()
        },
      })
    })
  },

  vertify_admin() {
    this.data.isVerified = true;
    wx.cloud.callFunction({
      name: 'verify_isAdmin'
    }).then(res => {
      this.setData({
        isAdmin: res.result.data[0].admin
      })
    })
    if(getApp().globalData.userInfo.admin === false) {
      wx.showToast({
        title: '您还不是管理员',
        icon: 'error'
      },2000);
    }
    if(getApp().globalData.userInfo.admin === true) {
      this.setData({
        isAdmin: true
      })
      wx.showToast({
        title: '验证成功',
        icon: 'success'  
      },2000);
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.showLoading({
      title: '加载中',
    })
    wx.cloud.callFunction({
      name: 'get_cloud_prj',
    }).then(res => {
      wx.hideLoading({
        success: (res) => {
          
        },
      })
      this.setData({
        prjs: res.result.data
      })
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})