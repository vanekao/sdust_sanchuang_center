const db = wx.cloud.database()
const _ = db.command
// pages/manage/manage.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    name: '',
    in_lab: false,
    picker: ['人工智能实验室', '物联网实验室', '虚拟现实实验室', '工业视觉实验室', '信息安全实验室'],
  },

  navigate(e) {
    var target = e.currentTarget.dataset.target
    if(this.data.in_lab === false) {
      wx.showToast({
        title: '请先加入实验室',
        icon: 'error'
      })
      return;
    }
    wx.navigateTo({
      url: '../' + target + '/' + target
    })
  },

  pickerChange(e) {
    console.log(e);
    this.setData({
      index: e.detail.value
    })
  },

  changeShow() {
    this.setData({
      show: true
    })
  },
  
  submit(e) {
    var that = this
    wx.showLoading({
      title: '正在提交',
    })
    var detail = e.detail.value.detail
    var lab = e.detail.value.lab
    if(lab === undefined) {
      wx.showToast({
        title: '请选择实验室',
      })
    }
    if(lab == 0) {
      lab = '人工智能实验室'
    } else if(lab == 1) {
      lab = '物联网实验室'
    } else if(lab == 2) {
      lab = '虚拟现实实验室'
    } else if(lab == 3) {
      lab = '工业视觉实验室'
    } else if(lab == 4) {
      lab = '信息安全实验室'
    }

    db.collection('userlist').where({
      _openid: getApp().globalData.userInfo._openid
    }).get().then(res => {
      var userInfo = res.data[0]
      var apply = userInfo.apply
      if(apply === undefined) {
        apply = []
      }

      for(var i = 0; i < apply.length; i++) {
        if(JSON.stringify(apply[i]).search('join_lab') != -1) {
          wx.showToast({
            title: '请勿重复申请',
            icon: 'error'
          })
          return;
        }
      }
      apply.push({
        type: 'join_lab',
        name: getApp().globalData.userInfo.name,
        lab: lab,
        detail: detail,
        read: false
      })

      db.collection('apply').add({
        data: {
          type: 'join_lab',
          name: getApp().globalData.userInfo.name,
          lab: lab,
          detail: detail,
          read: false
        }
      }).then(res => {
        console.log(res);
      })

      db.collection('userlist').where({
        _openid: getApp().globalData.userInfo._openid
      }).update({
        data: {
          apply: _.set(apply)
        }
      }).then(res => {
        wx.hideLoading({
          success: (res) => {
            wx.showToast({
              title: '提交成功',
              icon: 'success'
            })
            that.setData({
              show: false
            })
          },
        })
      })
    })
    
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var lab = getApp().globalData.userInfo.in_lab
    if(lab != '') {
      this.setData({
        in_lab: lab
      })
    }
    var name = getApp().globalData.userInfo.name
    this.setData({
      name: name
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})