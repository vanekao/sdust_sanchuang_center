import { product } from './data.js';
// pages/ailab/ailab.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    showBtnLoading: false,
    btnTitle: '寻找终端',
    show_popup: false,
    terminal_img: 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fimages2015.cnblogs.com%2Fblog%2F858860%2F201606%2F858860-20160613180735338-570381810.jpg&refer=http%3A%2F%2Fimages2015.cnblogs.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1624157871&t=7b2eff0922e5371e8f52cb5b74fe47ac',
    device_item: [
      {img: 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fimgmall.tg.com.cn%2Fgroup1%2FM00%2F81%2F80%2FCgooalWnoHeGffufAADWHue7lEw291.jpg&refer=http%3A%2F%2Fimgmall.tg.com.cn&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1624156759&t=d7974ff8a3a9c59c06900640058e63b9', text: '温控系统'},
      {img: 'https://ss2.bdstatic.com/70cFvnSh_Q1YnxGkpoWK1HF6hhy/it/u=3780966470,2837820310&fm=224&gp=0.jpg', text: '排风系统'},
      {img: 'https://ss2.bdstatic.com/70cFvnSh_Q1YnxGkpoWK1HF6hhy/it/u=1419281837,484644621&fm=26&gp=0.jpg', text: '实验室监控'},
      {img: '', text: ''},
      {img: '', text: ''},
      {img: '', text: ''},
      {img: '', text: ''},
    ]
  },

  show() {
    this.setData({
      show_popup: true
    })
  },

  /**
   * 初始化蓝牙设备
   */
  initBlue:function(){
    var that = this;
    wx.showToast({
      title: '即将上线',
      icon: 'error'
    })
    return;
    wx.openBluetoothAdapter({     //调用微信小程序api 打开蓝牙适配器接口
      success: function (res) {
        that.setData({
          showBtnLoading: true,
          btnTitle: '正在搜索'
        });
        setTimeout(()=>{
          that.setData({
            btnTitle: '寻找终端'
          });
          wx.showToast({
            title: '未找到设备',
            icon: 'error',
            duration: 2000
          })
          that.setData({
            showBtnLoading: false
          });
        }, 5000);
      },
      fail: function (res) {    //如果手机上的蓝牙没有打开，可以提醒用户
        wx.showToast({
          title: '请开启蓝牙',
          icon: 'error',
          duration: 2000
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.lin.renderWaterFlow(this.data.device_item, false ,()=>{
      console.log('渲染成功')
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})