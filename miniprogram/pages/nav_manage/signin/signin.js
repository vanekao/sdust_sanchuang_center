// pages/signin/signin.js
const db = wx.cloud.database()
const _ = db.command
Page({
  /**
   * 页面的初始数据
   */
  data: {
    show_popup: false,
    released_signin: [],
    signed: []
  },

  scanCode() {
    var that = this
    wx.scanCode({
      onlyFromCamera: false,
      scanType: [],
      success: (result) => {
        console.log(result);
        that.toSignin(result.result)
      },
      fail: (res) => {},
      complete: (res) => {},
    })
  },

  showQRcode(e) {
    var code = e.currentTarget.dataset.code;
    wx.previewImage({
      urls: ['https://api.pwmqr.com/qrcode/create/?url='+code],
    })
  },

  show() {
    this.setData({
      show_popup: true
    })
  },

  toSignin(e) {
    var code = null
    if(e.detail === undefined) {
      code = e
    } else {
      code = e.detail.value.signinCode;
    }
    if(code === '') {
      wx.showToast({
        title: '请输入签到码',
        icon: 'error'
      })
      return;
    }
    wx.showLoading({
      title: '签到中',
    })
    console.log(code);
    db.collection('sign_in_list').where({
      code: code
    }).get().then(res => {
      console.log(res.data[0])
      if(res.data[0] === undefined) {
        wx.hideLoading({
          success: (res) => {
            wx.showToast({
              title: '签到码错误',
              icon: 'error'
            })
          },
        })
        return;
      }
      var openid = res.data[0]._openid
      var released = []

      db.collection('userlist').where({
        _openid: openid
      }).get().then(res => {
        console.log(res);
        released = res.data[0].released_signin
        
        var index = 0;

        for(var i = 0; i < released.length; i++) {
          console.log(released[i]);
          if(released[i].code === code) {
            console.log(true);
            index = i;
          }
        }

        var date = new Date()
        var year = date.getFullYear()
        var month = date.getMonth() + 1
        var day = date.getDate()
        var hour = date.getHours()
        var minute = date.getMinutes()
        var people_name = getApp().globalData.userInfo.name

        if(released[index].people === undefined) {  
          //将released[index]字符化
          var info = JSON.stringify(released[index])
          var push = "\"people\":[]"
          //将"people":[]添加至JSON字符串
          info = info.substring(0, info.length - 1) + "," + push + "}"
          released[index] = JSON.parse(info)
          //以下代码向people数组里添加一个对象
          info = JSON.stringify(released[index])
          push = "{\"name\":\""+people_name+"\",\"time\":{\"year\":\""+year+"\",\"month\":\""+month+"\",\"day\":\""+day+"\",\"hour\":\""+hour+"\",\"minute\":\""+minute+"\"}}"
          console.log(info);
          info = info.substring(0, info.length - 2) + push + "]}"
          released[index] = JSON.parse(info)
        } else {
          released[index].people.push({
            name: people_name,
            time: {
              year: year,
              month: month,
              day: day,
              hour: hour,
              minute: minute
            }
          })
        }
        console.log(released[index]);

        db.collection('userlist').where({
          _openid: openid
        }).update({
          data: {
            released_signin: _.set(released)
          }
        }).then(res => {
          db.collection('userlist').where({
            _openid: getApp().globalData.userInfo._openid
          }).get().then(res => {
            var signed_list = res.data[0].signed
            if(signed_list === undefined) {
              console.log("undefined");
              signed_list = []
            }
            console.log(111);
            signed_list.push(released[index])
            console.log(signed_list);
  
            db.collection('userlist').where({
              _openid: getApp().globalData.userInfo._openid
            }).update({
              data: {
                signed: _.set(signed_list)
              }
            })
  
            wx.hideLoading({
              success: (res) => {
                wx.showToast({
                  title: '签到成功',
                  icon: 'success'
                })
                this.dataShow()
              },
            })


          })
        })
      })
    })
  },

  getRandomCode() {
    let code = "";
    const array = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 
    'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 
    'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 
    'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
  
    for (let i = 0; i < 6; i++) {
      let id = Math.round(Math.random() * 61);
      code += array[id];
    }
    return code;
  },

  generateCode() {
    let promise = new Promise(function(resolve, reject) {
      let code = getRandomCode();
  
      queryCode(code).then(res => {
        if (res[0]) {
          //  需要结束
          resolve(res[1]);
        } else {
          //  重复，不能结束，进行递归
          return generateCode();
        }
      });
    });
  
    return promise;
  },

  release(e) {
    var title = e.detail.value.title;
    if(title === '') {
      wx.showToast({
        title: '请输入标题',
        icon: 'error'
      })
      return;
    }
    wx.showLoading({
      title: '正在加载',
    })

    var date = new Date();
    var year = date.getFullYear()
    var month = date.getMonth() + 1
    var day = date.getDate()
    var hour = date.getHours()
    var minute = date.getMinutes()
    var sec = date.getSeconds()
    
    if ((month + '').length === 1) {
      month = '0' + month;
    }
    if ((day + '').length === 1) {
      day = '0' + day;
    }
    if ((minute + '').length === 1) {
      minute = '0' + minute;
    }
    if ((sec + '').length === 1) {
      sec = '0' + sec;
    }

    var code = this.getRandomCode()
    this.setData({
      code: code
    })

    db.collection('sign_in_list').add({
      data: {
        code: code
      }
    }).then(res => {
      console.log('上传至签到列表：');
      console.log(res);
    })

    var release_time = year + '-' + month + '-' + day + ' ' + hour + ':' + minute + ':' + sec
    wx.cloud.callFunction({
      name: 'release_signin',
      data: {
        title: title,
        code: code,
        time: release_time,
        author: getApp().globalData.userInfo.name
      }
    }).then(res => {
      console.log(res);
      wx.hideLoading({
        success: (res) => {
          this.setData({
            show_popup: false
          })
          wx.showToast({
            title: '发布成功',
            icon: 'success'
          })
          this.onLoad();
        },
      })
    })
  },

  dataShow() {
    wx.showLoading({
      title: '加载中',
    })
    wx.cloud.callFunction({
      name: 'get_signin'
    }).then(res => {
      console.log(res);
      this.setData({
        released_signin: res.result.data[0].released_signin,
        signed: res.result.data[0].signed
      })
      wx.hideLoading({
        success: (res) => {
          
        },
      })
    })
    
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.dataShow();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    // this.dataShow()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})