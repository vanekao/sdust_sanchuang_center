// pages/add_prj_detail/add_prj_detail.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    prj_id: null
  },

  add(res) {
    wx.showLoading({
      title: '添加中',
    })
    var text = res.detail.value.text;
    var _id = this.data.prj_id;
    var author = getApp().globalData.userInfo.name;
    var date = new Date();
    var minute = date.getMinutes();
    var hour = date.getHours();
    // minute = (Array(2).join(0) + item.hour).slice(-n);
    if(('' + minute).length == 1) {
      minute = '0' + minute;
    }
    if(('' + hour).length == 1) {
      hour = '0' + hour;
    }

    wx.cloud.callFunction({
      name: 'add_prj_detail',
      data: {
        prj_id: _id,
        text: text,
        author: author,
        year: date.getFullYear() - 2000,
        month: date.getMonth() + 1,
        day: date.getDate(),
        hour: hour,
        minute: minute
      }
    }).then(res => {
      wx.hideLoading({
        success: (res) => {
          wx.navigateBack({
            delta: 1
          })
        },
      })
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      prj_id: options.id
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})