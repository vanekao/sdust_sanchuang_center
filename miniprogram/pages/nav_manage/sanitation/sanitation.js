// pages/sanitation/sanitation.js
const db = wx.cloud.database()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    picker: ['人工智能实验室', '物联网实验室', '虚拟现实实验室', '工业视觉实验室', '信息安全实验室'],
    index: '',
    show: false,
    record: []
  },

  add(e) {
    wx.showLoading({
      title: '正在提交',
    })
    console.log(e);
    var lab = e.detail.value.lab
    if(lab == 0) {
      lab = '人工智能实验室'
    } else if(lab == 1) {
      lab = '物联网实验室'
    } else if(lab == 2) {
      lab = '虚拟现实实验室'
    } else if(lab == 3) {
      lab = '工业视觉实验室'
    } else if(lab == 4) {
      lab = '信息安全实验室'
    }
    console.log(lab);
    var date = new Date()
    var year = date.getFullYear()
    var month = date.getMonth()
    var day = date.getDate()
    var hour = date.getHours()
    var minute = date.getMinutes()
    var name = e.detail.value.name

    if(('' + minute).length == 1) {
      minute = '0' + minute;
    }
    if(('' + hour).length == 1) {
      hour = '0' + hour;
    }

    var detail = e.detail.value.detail
    db.collection('santation').add({
      data: {
        lab: lab,
        name: name,
        detail: detail,
        year: year,
        month: month,
        day: day,
        hour: hour,
        minute: minute
      }
    }).then(res => {
      console.log(res);
      wx.hideLoading({
        success: (res) => {
          wx.showToast({
            title: '添加成功',
            icon: 'success'
          })
          this.setData({
            show: false
          })
          this.onLoad()
        },
      })
    }).then(err => {
      console.log(err);
    })
  },

  changeShow() {
    this.setData({
      show: true
    })
  },

  pickerChange(e) {
    console.log(e);
    this.setData({
      index: e.detail.value
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.showLoading({
      title: '正在加载',
    })
    db.collection('santation').get().then(res=> {
      this.setData({
        record: res.data
      })
    }).then(res => {
      wx.hideLoading({
        success: (res) => {
          
        },
      })
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})