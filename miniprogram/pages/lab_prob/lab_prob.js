const db = wx.cloud.database()
// pages/advice/advice.js
Page({
 
  /** 页面的初始数据*/
  data: {
    content:'',
    phone:'',
    name:'',
    advice:'',
    pics:[],
    isShow: true
  },
  /**获取textarea值：评论内容 */
  bindTextAreaBlur:function(e){
    this.setData({
      advice:e.detail.value
    })
  },
  /**获取手机或邮箱*/
  inputPhone: function (e) {
    this.setData({
      phone: e.detail.value
    })
  },
  /**获取称呼 */
  inputName: function (e) {
    this.setData({
      name: e.detail.value
    })
  },
 
  /**上传图片 */
  uploadImage:function(){
    let that=this;
    let pics = that.data.pics;
    wx.chooseImage({
      count:3 - pics.length,
      sizeType: ['original', 'compressed'], 
      sourceType: ['album', 'camera'], 
      success: function(res) {
        let imgSrc = res.tempFilePaths;
         pics.push(imgSrc);
        if (pics.length >= 3){
          that.setData({
            isShow: false
          }) 
        }
       that.setData({
          pics: pics
        })
      },
    })
 
  },
 
  /**删除图片 */
  deleteImg:function(e){
    let that=this;
    let deleteImg=e.currentTarget.dataset.img;
    let pics = that.data.pics;
    let newPics=[];
    for (let i = 0;i<pics.length; i++){
     //判断字符串是否相等
      if (pics[i]["0"] !== deleteImg["0"]){
        newPics.push(pics[i])
      }
    }
    that.setData({
      pics: newPics,
      isShow: true
    })
    
  },
 
  /**提交 */
  submit:function(){
    wx.showLoading({
      title: '正在提交',
    })
    let advice = this.data.advice
    let name=this.data.name
    let contact=this.data.phone
    let pics=this.data.pics

    //保存操作
    db.collection('lab_problems').add({
      data: {
        advice: advice,
        pics: pics,
        contact: contact,
        name: name
      }
    }).then(res => {
      console.log(res);
      wx.hideLoading({
        success: (res) => {
          wx.showToast({
            title: '提交成功',
            icon: 'success'
          })
        },
      })
    })
  }
})