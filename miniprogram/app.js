//app.js
App({
  onLaunch: function () {
    this.globalData = {
      isLogin: null, //是否登录
      openid: null,
      userInfo: {
        openid: null, //openID
        nickName: null, //用户昵称
        avatarUrl: null, //用户头像链接
        name: "", //真实姓名
        gender: "", //性别
        college: "", //学院
        class: "", //专业年级
        studentID: "", //学号
        phoneNumber: "", //手机号
        province: "", //籍贯
        politicalStatus: "", //政治面貌,
        admin: null,
        in_lab: '' //已加入实验室
      }
    }
    console.log(11)
    this.globalData.isLogin = wx.getStorageSync('isLogin')
    if (!wx.cloud) {
      console.error('请使用 2.2.3 或以上的基础库以使用云能力')
    } else {
      wx.cloud.init({
        // env: 'cloud1-4gaojii5e1c8c56e',
        traceUser: true,
      })
      const db = wx.cloud.database();
      const _ = db.command;
      //获取openid
      console.log(2)
      wx.cloud.callFunction({
        name: 'login'
      }).then(res => {
        console.log(res)
        this.globalData.openid = res.result.openid
        //获取数据库用户信息
        db.collection('userlist').where({
          _openid: this.globalData.openid
        }).get().then(res => {
          console.log(res)
          this.globalData.userInfo = Object.assign(this.globalData.userInfo, res.data[0])
          if (res.data[0] !== undefined) {
            wx.setStorageSync('isLogin', true)
          }
        })
      })


    }
  }
})