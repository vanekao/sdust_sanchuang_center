// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()
const db = cloud.database()

// 云函数入口函数
exports.main = async (event, context) => {
  db.collection('seats').doc(event.id).update({
    data: {
      rsvDate: event.rsvDate,
      rsvStartTime: event.rsvStartTime,
      rsvEndTime: event.rsvEndTime
    }
  })
}