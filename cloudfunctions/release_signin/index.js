// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()
const db = cloud.database()
const _ = db.command

// 云函数入口函数
exports.main = async (event, context) => {
  db.collection('userlist').where({
    _openid: event.userInfo.openId
  }).update({
    data: {
      released_signin: _.push({
        title: event.title,
        code: event.code,
        time: event.time,
        author: event.author
      })
    }
  })
  return event;
}