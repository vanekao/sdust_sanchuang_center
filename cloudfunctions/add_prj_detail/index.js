// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()
const db = cloud.database()
const _ = db.command;

// 云函数入口函数
exports.main = async (event, context) => {
  var _id = event.prj_id;
  var text = event.text;
  db.collection('cloud_prj').where({
    _id: _id
  }).update({
    data: {
      prj_detail: _.push({
        text: text,
        author: event.author,
        year: event.year,
        month: event.month,
        day: event.day,
        hour: event.hour,
        minute: event.minute
      })
    }
  }).then(res => {
    return res
  })
  // return event;
}