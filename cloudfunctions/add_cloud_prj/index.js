// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()
const db = cloud.database()

// 云函数入口函数
exports.main = async (event, context) => {
  var openId = event.userInfo.openId;
  db.collection('cloud_prj').add({
    data: {
      openId: openId,
      initiator: event.value.initiator,
      introduction: event.value.introduction,
      member: event.value.member,
      name: event.value.name,
      remote_repo: event.value.remote_repo,
      prj_detail: []
    }
  }).then(res => {
    return res;
  }).then(err => {
    return err;
  })
}